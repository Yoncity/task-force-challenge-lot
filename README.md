# LOT

This project is developed purely with html, css and javascript.

[Demo](https://task-force-challenge-lot.netlify.app)

## How to run.

- First, clone the web-app.
- Simply, open the **index.html** page.
